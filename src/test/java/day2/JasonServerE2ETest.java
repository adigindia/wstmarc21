package day2;

import static org.junit.Assert.*;
//import static org.testng.Assert.assertEquals;

import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class JasonServerE2ETest {

	int id_var; // this is member variable // env variable
	
	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void test() {
		
		createEmployee();
		
		updateEmployee();
		
		deleteEmployee();
	}

	public void createEmployee()
	{
		// save the id value in id_var
		

		RequestSpecification req = RestAssured.given();
		req.header("Content-type", "application/json");
		
		
		JSONObject requestParams = new JSONObject();

		requestParams.put("name", "AdityaRATrial1"); // Key Value here
		requestParams.put("salary", "10000"); // Key Value here

		System.out.println("Body Json object = " + requestParams.toJSONString());
		
		req.body(requestParams.toJSONString()); // attach json to body
		
		
		Response res = req.post("http://localhost:3000/employees"); 
		
		
		System.out.println("Response = " + res.prettyPrint());
		
		
		int statusCode = res.getStatusCode();
		
		System.out.println("Status code = " + statusCode);
		
		JsonPath jsonRes = new JsonPath(res.body().asString());
		
		int id = jsonRes.get("id");
		
		System.out.println("Id of the new employee = " + id);

		id_var = id;
		
	}
	
	public void updateEmployee()
	{
		// use the id_var in the update
		
		System.out.println("In Update var = " + id_var);
		
		
		RequestSpecification req = RestAssured.given();
		req.header("Content-type", "application/json");
		
		
		JSONObject requestParams = new JSONObject();

		requestParams.put("name", "AdityaUpdatedRA"); // Key Value here
		requestParams.put("salary", "10000RA"); // Key Value here

		System.out.println("Body Json object = " + requestParams.toJSONString());
		
		req.body(requestParams.toJSONString()); // attach json to body
		
		String url = "http://localhost:3000/employees/"+id_var;
		
		System.out.println("In Update var = URL = " + url);
		
		Response res = req.put(url); 
		
		
		System.out.println("Response = " + res.prettyPrint());
		
		
		int statusCode = res.getStatusCode();
		
		System.out.println("Status code = " + statusCode);
		
		JsonPath jsonRes = new JsonPath(res.body().asString());
		
		int id = jsonRes.get("id");

		String name = jsonRes.get("name");

		System.out.println("Id of the updated employee = " + id);
		
		// assert that the id for the updated employee is the same
		
		assertEquals( id_var, id); // int, boolean, String
		
		assertEquals( "AdityaUpdatedRA",name); // int, boolean, String
		
		
	}

	public void deleteEmployee()
	{
		// use the id_var in delete
		System.out.println("In delete var = " + id_var);
		
		RequestSpecification req = RestAssured.given();
		req.header("Content-type", "application/json");
	
		Response res = req.delete("http://localhost:3000/employees/" + id_var); 
		
		System.out.println("Response = " + res.prettyPrint());
		int statusCode = res.getStatusCode();
		System.out.println("Status code = " + statusCode);


	}

	
	@After
	public void tearDown() throws Exception {
	}


}
