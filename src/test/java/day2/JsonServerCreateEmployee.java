package day2;

import static org.junit.Assert.*;

import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class JsonServerCreateEmployee {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {

		RequestSpecification req = RestAssured.given();
		req.header("Content-type", "application/json");
		
		
		JSONObject requestParams = new JSONObject();

		requestParams.put("name", "Aditya"); // Key Value here
		requestParams.put("salary", "10000"); // Key Value here

		System.out.println("Body Json object = " + requestParams.toJSONString());
		
		req.body(requestParams.toJSONString()); // attach json to body
		
		
		Response res = req.post("http://localhost:3000/employees"); 
		
		
		System.out.println("Response = " + res.prettyPrint());
		
		
		int statusCode = res.getStatusCode();
		
		System.out.println("Status code = " + statusCode);
		
		JsonPath jsonRes = new JsonPath(res.body().asString());
		
		int id = jsonRes.get("id");
		
		System.out.println("Id of the new employee = " + id);

	
	}

	
	@After
	public void tearDown() throws Exception {
	}


}
