package day2;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class StudentSearchExercise1 {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
	
		RequestSpecification req = RestAssured.given();
		req.header("Content-type", "application/json");
		
		Response res = req.get("http://104.197.206.206:8080/students/all"); 
		
		System.out.println("Response = " + res.prettyPrint());
		int statusCode = res.getStatusCode();
		
		System.out.println("Status code = " + statusCode);
		
		JsonPath jsonRes = new JsonPath(res.body().asString());
		
		List<Integer> allIds = jsonRes.get("studentId");
		List<String> allnames = jsonRes.get("firstName");
		List<Integer> allyob = jsonRes.get("dateOfBirth.year");
		
		
		int length = allIds.size();
		
		for (int i = 0 ; i < length ; i++) {
			
			int id = allIds.get(i);
			String name = allnames.get(i);
			int yob =  allyob.get(i);
			System.out.println("Id = " + id + ": " + "Name = " + name + ": " + "YOB = " + yob );
			
		}

	}	
	
	
	@After
	public void tearDown() throws Exception {
	}


}
