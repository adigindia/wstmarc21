package day2;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class JsonServerRequests {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
	
		RequestSpecification req = RestAssured.given();
		//req.header("Content-type", "application/json");
		
		Response res = req.get("http://localhost:3000/employees"); 
		
		
		System.out.println("Response = " + res.prettyPrint());
		
		
		int statusCode = res.getStatusCode();
		
		System.out.println("Status code = " + statusCode);
		
		JsonPath jsonRes = new JsonPath(res.body().asString());
		
		List<Integer> allIds = jsonRes.get("id");
		
		int length = allIds.size();
		
		for (int i = 0 ; i < length ; i++) {
			
			int id = allIds.get(i);
			System.out.println("ID =" + id);
		}
		
		List<String> allnames = jsonRes.get("name");
		
		for (int i = 0 ; i < allnames.size()  ; i++) {
			
			String name = allnames.get(i);
			System.out.println("name =" + name);
		}
		
		List<String> allsalary = jsonRes.get("salary");
		
		for (int i = 0 ; i < allsalary.size()  ; i++) {
			
			String salary = allsalary.get(i);
			System.out.println("salary =" + salary);
		}
		
	

	}
	
	@After
	public void tearDown() throws Exception {
	}


}
