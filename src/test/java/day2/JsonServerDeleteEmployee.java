package day2;

import static org.junit.Assert.*;

import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class JsonServerDeleteEmployee {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {

		RequestSpecification req = RestAssured.given();
		req.header("Content-type", "application/json");
	
		Response res = req.delete("http://localhost:3000/employees/54"); 
		
		System.out.println("Response = " + res.prettyPrint());
		int statusCode = res.getStatusCode();
		System.out.println("Status code = " + statusCode);

	}

	
	@After
	public void tearDown() throws Exception {
	}


}
