package day2;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HelloWorldJunit {

	@Before
	public void setUp() throws Exception {
		// this will get executed first
		// setup // preconditions
		System.out.println("Inside setup");
	}

	@Test
	public void test() {
		// fail("Not yet implemented");
		// all the tests can be done
		/*
		 * this is a multi line comment
		 */
		System.out.println("Inside Test");
		System.out.println("HelloWorld Junit");
		
	}

	@After
	public void tearDown() throws Exception {
		
		// closures/ shutdowns,/ teardown / postcondition
		System.out.println("inside tearDown");
	}


}
